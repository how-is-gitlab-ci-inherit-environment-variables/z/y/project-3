# project-3

![Variables inside project](https://i.ibb.co/7tw2CxN/Screenshot-2020-03-05-at-11-56-23.png)

## [Gitlab CI Return](https://gitlab.com/how-is-gitlab-ci-inherit-environment-variables/z/y/project-3/pipelines/123486380)

- echo:
    ```bash
    $ echo $MSG
     project-3
    Job succeeded
    ```

- echo with vars:
    ```bash
    $ echo $MSG
     project-3
    Job succeeded
    ```
    
### Explain
The custom value in .gitlab-ci.yml doesn't work, because group's settings is the most important!

if group $MSG was not defined for group Y, then we would see Z

And until the tree of groups ends